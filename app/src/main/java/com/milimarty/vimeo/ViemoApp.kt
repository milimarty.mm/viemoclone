package com.milimarty.vimeo

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.milimarty.vimeo.utils.Utils
import com.milimarty.news.utils._contextG
import com.milimarty.news.utils.phoneWidth
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ViemoApp : Application() {
    override fun onCreate() {
        super.onCreate()
        /**
         * Change theme to DayLight
         */
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        /**
         * init GLOBAL CONTEXT
         */
        _contextG = applicationContext
        phoneWidth = Utils.getPhoneWidth()

    }
}