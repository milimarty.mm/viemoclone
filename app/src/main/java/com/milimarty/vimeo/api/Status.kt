package com.milimarty.vimeo.api

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}