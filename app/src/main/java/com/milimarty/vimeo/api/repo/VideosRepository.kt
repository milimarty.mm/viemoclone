package com.milimarty.vimeo.api.repo


import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.milimarty.news.utils.PARE_PAGE
import com.milimarty.vimeo.api.VimeoApi
import com.milimarty.vimeo.areas.videoslist.repo.VideosPagingSource
import javax.inject.Inject


class VideosRepository @Inject constructor(private val vimeoApi: VimeoApi) {


    fun searchVideos(searchText: String) =
        Pager(
            config = PagingConfig(
                pageSize = PARE_PAGE,
                maxSize = 80,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {
                VideosPagingSource(vimeoApi, searchText)
            }
        ).liveData


}