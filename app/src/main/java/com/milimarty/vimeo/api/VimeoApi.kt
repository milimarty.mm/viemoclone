package com.milimarty.vimeo.api

import com.milimarty.vimeo.areas.videoslist.model.VideoItemModel
import com.milimarty.news.utils.PARE_PAGE
import com.milimarty.vimeo.areas.videodetails.model.VideoStreamConfig
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface VimeoApi {


    @GET("videos/")
    suspend fun searchVideo(
        @Query("query") query: String,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int = PARE_PAGE,
    ): VideoItemModel


    @GET
    suspend fun getVideoStreamConfig(
        @Url url: String
    ): Response<VideoStreamConfig>


}