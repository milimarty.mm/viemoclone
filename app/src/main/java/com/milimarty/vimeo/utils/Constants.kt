package com.milimarty.news.utils

import android.content.Context


    /**
     * Fonts
     */
    const val customMediumFont:String = "font/sf_font.otf"
    const val customBoldFont:String = "font/sf_bold_font.otf"

    /**
     * Global Context
     */
    lateinit var _contextG: Context


    const val PARE_PAGE:Int = 10

    const val FIRST_PAGE_INDEX = 1

    const val DEFAULT_SEARCH_TEXT = ""

    var phoneWidth:Int = 0


