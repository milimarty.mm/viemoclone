package com.milimarty.vimeo.utils

import android.content.Context.INPUT_METHOD_SERVICE
import android.content.res.Resources
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.milimarty.news.utils._contextG
import java.lang.StringBuilder
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


object Utils {

    fun resourceDpToPx(recourseID: Int): Float {
        return (_contextG.resources.getDimension(recourseID) / _contextG.resources.displayMetrics.density)
    }

    fun dpToPx(dip: Float) = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dip,
        _contextG.resources.displayMetrics
    )

    fun getPhoneWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }

    fun hideSoftKeyboard(view: View?) {
        if (view != null) {
            val inputManager: InputMethodManager = view.context
                .getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun getVideoDuration(second: Long): String {
        val hours = second / 3600
        val minutes = (second % 3600) / 60
        val seconds = second % 60
        val videoDuration = StringBuilder("")

        if(hours>0){
            videoDuration.append(hours).append(":")
        }
        if(minutes>0){
            videoDuration.append(minutes).append(":")
        }
        videoDuration.append(seconds)

        return videoDuration.toString()

    }

}