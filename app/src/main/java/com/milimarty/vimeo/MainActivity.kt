package com.milimarty.vimeo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    var onBackPressed: () -> Boolean = { true }
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.lightTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onBackPressed() {
        if(onBackPressed.invoke())
            super.onBackPressed()
    }
}