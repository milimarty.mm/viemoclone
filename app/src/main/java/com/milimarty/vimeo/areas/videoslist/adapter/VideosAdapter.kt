package com.milimarty.vimeo.areas.videoslist.adapter

import android.R.attr.*
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.milimarty.vimeo.R
import com.milimarty.vimeo.areas.videoslist.model.VideoItemModel
import com.milimarty.vimeo.databinding.VideoItemCardViewBinding


class VideosAdapter(private val  onItemClickListener:(VideoItemModel.VideoDetails)->Unit) :
    PagingDataAdapter<VideoItemModel.VideoDetails, VideosAdapter.VideoItemViewHolder>(diffUtils) {

    companion object {
        private val diffUtils = object : DiffUtil.ItemCallback<VideoItemModel.VideoDetails>() {
            override fun areItemsTheSame(
                oldItem: VideoItemModel.VideoDetails,
                newItem: VideoItemModel.VideoDetails
            ): Boolean {
                return oldItem.title == newItem.title
            }

            override fun areContentsTheSame(
                oldItem: VideoItemModel.VideoDetails,
                newItem: VideoItemModel.VideoDetails
            ): Boolean {
                return oldItem == newItem
            }


        }
    }

    override fun onBindViewHolder(holder: VideoItemViewHolder, position: Int) {
        val item = getItem(position)
        item?.let {
            holder.bind(it)
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoItemViewHolder {
        val binding =
            VideoItemCardViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VideoItemViewHolder(binding)

    }


    inner class VideoItemViewHolder(private val binding: VideoItemCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                val position =  bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val item = getItem(position)
                    item?.let {
                        onItemClickListener(it)
                    }
                }
            }

        }

        @SuppressLint("CheckResult")
        fun bind(videoDetailsItem: VideoItemModel.VideoDetails) {
            binding.videoDetailsItem = videoDetailsItem

            /**
             *  Glide for load image from url to image view
             *  @param binding.photoItemImageView
             */
            Glide
                .with(binding.videoItemImageView)
                .load(videoDetailsItem.imageUrl)
                .centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade())
                .error(R.drawable.photo_place_holder_img)
                .into(binding.videoItemImageView)
        }

    }


}