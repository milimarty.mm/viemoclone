package com.milimarty.vimeo.areas.videodetails.repo

import android.util.Log
import androidx.lifecycle.LiveData
import com.google.gson.Gson
import com.milimarty.vimeo.api.VimeoApi
import com.milimarty.vimeo.areas.videodetails.NetworkResponse
import kotlinx.coroutines.*
import org.json.JSONObject
import javax.inject.Inject

class VideoDetailsRepository @Inject constructor(private val vimeoApi: VimeoApi) {
    private var getLockedLoopsListJob: CompletableJob? = null

    fun getVideoStreamConfig(videoId: Long): LiveData<NetworkResponse<String, Any>> {
        getLockedLoopsListJob = Job()
        return object : LiveData<NetworkResponse<String, Any>>() {
            override fun onActive() {
                super.onActive()
                getLockedLoopsListJob?.let {
                    CoroutineScope(Dispatchers.IO + it).launch {
                        val response =
                            vimeoApi.getVideoStreamConfig("https://player.vimeo.com/video/$videoId/config")
                        Log.d("TAGGGG", response.body().toString())
                        if (response.isSuccessful) {

                            withContext(Dispatchers.Main) {
                                value =
                                    NetworkResponse.Success(response.body()!!.request.files.hls.cdns.akfire.url)
                                it.complete()
                            }
                        } else {
                            withContext(Dispatchers.Main) {
                                value = NetworkResponse.ApiError("Error", response.code())
                                it.complete()
                            }
                        }
                    }
                }
            }
        }
    }
}