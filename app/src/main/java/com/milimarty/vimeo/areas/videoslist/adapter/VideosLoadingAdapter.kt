package com.milimarty.vimeo.areas.videoslist.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.milimarty.vimeo.databinding.VideoListLoadingFooterBinding

class VideosLoadingAdapter(val retryMethod: () -> Unit) :
    LoadStateAdapter<VideosLoadingAdapter.LoadStateViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadStateViewHolder {
        val binding =
            VideoListLoadingFooterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LoadStateViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)

    }


    inner class LoadStateViewHolder(private val binding: VideoListLoadingFooterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.videoListLoadingStateRetryButton.setOnClickListener {
                retryMethod()
            }
        }

        fun bind(loadState: LoadState) {
            binding.apply {
                videoListLoadingStateLoadingBar.isVisible = loadState is LoadState.Loading
                videoListLoadingStateRetryButton.isVisible = loadState !is LoadState.Loading
                videoListLoadingStateTextView.isVisible = loadState !is LoadState.Loading
            }
        }

    }


}