package com.milimarty.vimeo.areas.videoplayer

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.milimarty.vimeo.R
import com.milimarty.vimeo.databinding.FragmentVideoPlayerBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.exo_player_custom_controller.view.*


const val PLAY_WHEN_READY = "playWhenReady"
const val CURRENT_WINDOW = "currentWindow"
const val PLAY_BACK_POSITION = "playbackPosition"
const val IS_FULL_SCREEN = "IS_FULL_SCREEN"

@AndroidEntryPoint
class VideoPlayerFragment : Fragment(R.layout.fragment_video_player) {
    /**
     * video details
     */
    private val args by navArgs<VideoPlayerFragmentArgs>()

    private var _mBinding: FragmentVideoPlayerBinding? = null
    private val mBinding
        get() = _mBinding!!

    private var playWhenReady = true
    private var currentWindow = 0
    private var playbackPosition: Long = 0
    private var bundle: Bundle? = null


    private var player: SimpleExoPlayer? = null
    private var isFullscreen = false

    override fun onStart() {
        super.onStart()
        if (Util.SDK_INT >= 24) {
            playVideo()
        }
    }


    override fun onResume() {
        super.onResume()
        hideSystemUi()
        if (Util.SDK_INT < 24 || player == null) {
            playVideo()
        }
    }

    @SuppressLint("InlinedApi")
    private fun hideSystemUi() {
        mBinding.videoView.setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LOW_PROFILE
                    or View.SYSTEM_UI_FLAG_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        )
    }

    override fun onPause() {
        super.onPause()
        if (Util.SDK_INT < 24) {
            releasePlayer()
        }
    }

    override fun onStop() {
        super.onStop()
        if (Util.SDK_INT >= 24) {
            releasePlayer()
        }
    }

    private fun releasePlayer() {
        if (player != null) {
            playWhenReady = player!!.playWhenReady
            playbackPosition = player!!.currentPosition
            currentWindow = player!!.currentWindowIndex
            player!!.release()
            player = null
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(PLAY_WHEN_READY, playWhenReady)
        outState.putLong(PLAY_BACK_POSITION, playbackPosition)
        outState.putInt(CURRENT_WINDOW, currentWindow)
        outState.putBoolean(IS_FULL_SCREEN, isFullscreen)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _mBinding = FragmentVideoPlayerBinding.bind(view)
        bundle = savedInstanceState
        bundle?.let {
            currentWindow = it.getInt(CURRENT_WINDOW)
            playbackPosition = it.getLong(PLAY_BACK_POSITION)
            playWhenReady = it.getBoolean(PLAY_WHEN_READY)
            isFullscreen = it.getBoolean(IS_FULL_SCREEN)
        }

        orientationInit()


    }

    private fun orientationInit() {
        mBinding.videoView.exo_fullscreen_button.setOnClickListener {
            if (isFullscreen) {
                mBinding.videoView.exo_fullscreen_icon.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.exo_ic_fullscreen_enter

                    )
                )
                (activity as AppCompatActivity?)?.let {
                    it.supportActionBar?.show()
                }

                requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                val params = mBinding.videoView.layoutParams
                params.width = ViewGroup.LayoutParams.MATCH_PARENT
                params.height =
                    (200 * requireContext().resources
                        .displayMetrics.density).toInt()
                mBinding.videoView.layoutParams = params
                isFullscreen = false
            } else {
                mBinding.videoView.exo_fullscreen_icon.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.exo_ic_fullscreen_exit
                    )
                )
                (activity as AppCompatActivity?)?.let {
                    it.supportActionBar?.hide()
                }

                requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                val params = mBinding.videoView.layoutParams
                params.width = ViewGroup.LayoutParams.MATCH_PARENT
                params.height = ViewGroup.LayoutParams.MATCH_PARENT
                mBinding.videoView.layoutParams = params
                isFullscreen = true
            }
        }
    }

    private fun playVideo() {
        player = SimpleExoPlayer.Builder(requireContext()).build()
        val dataSourceFactory: DataSource.Factory = DefaultHttpDataSourceFactory()
        mBinding.videoView.player = player
        val hlsMediaSource: HlsMediaSource = HlsMediaSource.Factory(dataSourceFactory)
            .createMediaSource(MediaItem.fromUri(args.videoUrl!!))
        player?.let {
            it.addListener(object : Player.Listener {

                override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                    when (playbackState) {
                        ExoPlayer.STATE_READY -> mBinding.fragmentVideoPlayerLoading.isVisible =
                            false
                        ExoPlayer.STATE_BUFFERING -> mBinding.fragmentVideoPlayerLoading.isVisible =
                            true
                    }
                    super.onPlayerStateChanged(playWhenReady, playbackState)
                }


            })

            it.playWhenReady = playWhenReady
            it.setMediaSource(hlsMediaSource)
            it.seekTo(currentWindow, playbackPosition)
            it.prepare()
        }


    }

    override fun onDestroy() {
        _mBinding = null
        super.onDestroy()
    }


}