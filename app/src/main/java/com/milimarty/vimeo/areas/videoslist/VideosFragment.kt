package com.milimarty.vimeo.areas.videoslist

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.milimarty.vimeo.MainActivity
import com.milimarty.vimeo.R
import com.milimarty.vimeo.areas.videoslist.adapter.VideosAdapter
import com.milimarty.vimeo.areas.videoslist.adapter.VideosLoadingAdapter
import com.milimarty.vimeo.areas.videoslist.viewmodel.VideosViewModel
import com.milimarty.vimeo.databinding.FragmentVideosBinding
import com.milimarty.vimeo.utils.Utils.hideSoftKeyboard
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VideosFragment : Fragment(R.layout.fragment_videos) {
    /**
     * Values
     */
    private val viewModel by viewModels<VideosViewModel>()

    private var _mBinding: FragmentVideosBinding? = null
    private val mBinding
        get() = _mBinding!!

    private lateinit var videosAdapter: VideosAdapter





    private fun init() {

        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        /**
         * init Gallery Adapter
         */
        videosAdapter = VideosAdapter {
            val action = VideosFragmentDirections.actionVideosFragmentToVideoDetailsFragment(it)
            findNavController().navigate(action)
        }

        setupRecyclerView()
        /**
         * init search box
         */
        initSearchBox()

        /**
         * Loading View
         */
        initLoadState()


    }

    /**
     * init loading state
     */
    private fun initLoadState() {
        mBinding.fragmentVideosRetryButton.setOnClickListener {
            videosAdapter.retry()
        }

        videosAdapter.addLoadStateListener { it ->
            mBinding.apply {
                fragmentVideosRecyclerViewLoading.isVisible =
                    it.source.refresh is LoadState.Loading
                fragmentVideosRecyclerView.isVisible = it.source.refresh is LoadState.NotLoading
                fragmentVideosErrorTextView.isVisible = it.source.refresh is LoadState.Error
                fragmentVideosRetryButton.isVisible = it.source.refresh is LoadState.Error

                if (it.source.refresh is LoadState.NotLoading && it.append.endOfPaginationReached && videosAdapter.itemCount < 1) {
                    fragmentVideosRecyclerView.isVisible = false
                    fragmentVideosEmptyContainer.isVisible = true
                } else {
                    fragmentVideosEmptyContainer.isVisible = false

                }

            }
        }
    }

    /**
     *init search box
     */
    private fun initSearchBox() {
        (requireActivity() as MainActivity).onBackPressed = {
            if (mBinding.fragmentVideosSearchEditText.isFocused) {
                mBinding.fragmentVideosSearchEditText.clearFocus()
                false
            } else
                true

        }
        mBinding.fragmentVideosSearchEditText.apply {
            imeOptions = EditorInfo.IME_ACTION_SEARCH
            setOnEditorActionListener(OnEditorActionListener { v, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (v.text.toString().isNotEmpty() || v.text.toString() != " ") {
                        clearFocus()
                        mBinding.fragmentVideosRecyclerView.scrollToPosition(0)
                        viewModel.searchVideos(v.text.toString())
                        hideSoftKeyboard(mBinding.fragmentVideosSearchEditText)
                        return@OnEditorActionListener true
                    }
                    Toast.makeText(context, "please insert correct text .", Toast.LENGTH_SHORT)
                        .show()

                    return@OnEditorActionListener false
                }
                false
            })

        }

    }

    private fun setupRecyclerView() {


        mBinding.fragmentVideosRecyclerView.apply {


            hasFixedSize()
            adapter = videosAdapter.withLoadStateHeaderAndFooter(
                header = VideosLoadingAdapter { videosAdapter.retry() },
                footer = VideosLoadingAdapter { videosAdapter.retry() }
            )
            layoutManager = LinearLayoutManager(context)

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _mBinding = FragmentVideosBinding.bind(view)
        init()

        viewModel.newsList.observe(viewLifecycleOwner) {
            videosAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        }

    }


    override fun onDestroy() {
        super.onDestroy()
        _mBinding = null
    }
}