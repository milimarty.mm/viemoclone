package com.milimarty.vimeo.areas.videoslist.viewmodel

import androidx.lifecycle.*
import androidx.paging.cachedIn
import com.milimarty.news.utils.DEFAULT_SEARCH_TEXT
import com.milimarty.vimeo.api.repo.VideosRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class VideosViewModel @Inject constructor(private val repository: VideosRepository) :
    ViewModel() {

    private val searchTextLiveData = MutableLiveData(DEFAULT_SEARCH_TEXT)
    val newsList = searchTextLiveData.switchMap {
        repository.searchVideos(it).cachedIn(viewModelScope)
    }

    fun searchVideos(searchText: String) {
        searchTextLiveData.value = searchText
    }




}