package com.milimarty.vimeo.areas.videoslist.repo

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.milimarty.vimeo.areas.videoslist.model.VideoItemModel
import com.milimarty.news.utils.FIRST_PAGE_INDEX
import com.milimarty.vimeo.api.VimeoApi

import java.lang.Exception

class VideosPagingSource(
    private val vimeoApi: VimeoApi,
    private val searchText: String
) : PagingSource<Int, VideoItemModel.VideoDetails>() {


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, VideoItemModel.VideoDetails> {

        return try {
            val position = params.key ?: FIRST_PAGE_INDEX
            val response =   searchText.takeIf { it.isNotEmpty() && it.isNotBlank() }?.let {
                return@let vimeoApi.searchVideo(
                    query = searchText,
                    page = position,
                    perPage = params.loadSize
                )
            }?: kotlin.run{
                return@run  VideoItemModel(0,0, listOf())
            }

//            Log.d("RESPONSE-REQUEST", response.toString())

           return LoadResult.Page(
                data = response.videosList ,
                prevKey = if (position == FIRST_PAGE_INDEX) null else position - 1,
                nextKey = if (response.videosList.isEmpty()) null else position + 1

            )

        } catch (e: Exception) {
            e.printStackTrace()
            return  LoadResult.Error(e)
        }


    }

    override fun getRefreshKey(state: PagingState<Int, VideoItemModel.VideoDetails>): Int? {
        return state.anchorPosition
    }
}