package com.milimarty.vimeo.areas.videodetails.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class VideoStreamConfig(
    @SerializedName("request")
    val request: Request
) : Parcelable {
    @Parcelize
    data class Request(
        @SerializedName("files")
        val files: Files
    ) : Parcelable {
        @Parcelize
        data class Files(
            @SerializedName("hls")
            val hls: Hls
        ) : Parcelable {
            @Parcelize
            data class Hls(
                @SerializedName("cdns")
                val cdns: Cdns
            ) : Parcelable {
                @Parcelize
                data class Cdns(
                    @SerializedName("akfire_interconnect_quic")
                    val akfire: Akfire
                ) : Parcelable {
                    @Parcelize
                    data class Akfire(
                        @SerializedName("url")
                        val url: String
                    ) : Parcelable
                }
            }

        }
    }


}