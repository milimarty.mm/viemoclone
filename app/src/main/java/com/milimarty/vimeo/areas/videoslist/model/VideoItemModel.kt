package com.milimarty.vimeo.areas.videoslist.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VideoItemModel(
    @SerializedName("total")
    val totalResults: Long,
    @SerializedName("page")
    val page: Long,
    @SerializedName("data")
    val videosList: List<VideoDetails>
) : Parcelable {

    @Parcelize
    data class VideoDetails(
        @SerializedName("name")
        val title: String,
        @SerializedName("description")
        val description: String,
        @SerializedName("link")
        val url: String,
        @SerializedName("uri")
        val uri: String,
        @SerializedName("duration")
        val duration: Long,
        @SerializedName("pictures")
        val pictures: Pictures,
        @SerializedName("metadata")
        val metadata: VideoMetadata
    ) : Parcelable {

        val videoId: Long
            get() = uri.substringAfterLast("/").toLong()
        val imageUrl: String?
            get() = pictures.size.firstOrNull {
                it.height >= 360L
            }?.link
        val imageWithPlayButtonUrl: String?
            get() = pictures.size.firstOrNull {
                it.height >= 360L
            }?.linkWithPlayButton

        @Parcelize
        data class Pictures(
            @SerializedName("sizes")
            val size: List<PicturesSizes>
        ) : Parcelable {
            @Parcelize
            data class PicturesSizes(
                @SerializedName("width")
                val width: Long,
                @SerializedName("height")
                val height: Long,
                @SerializedName("link")
                val link: String,
                @SerializedName("link_with_play_button")
                val linkWithPlayButton: String,
            ) : Parcelable
        }

        @Parcelize
        data class VideoMetadata(
            @SerializedName("connections")
            val connections: Connections
        ) : Parcelable {
            @Parcelize
            data class Connections(
                @SerializedName("comments")
                val comments: Total,
                @SerializedName("likes")
                val likes: Total
            ) : Parcelable
            @Parcelize
            data class Total(
                @SerializedName("total")
                val total: Long,
            ) : Parcelable
        }
    }


}