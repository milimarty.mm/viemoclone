package com.milimarty.vimeo.areas.videodetails

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.milimarty.vimeo.R
import com.milimarty.vimeo.areas.videodetails.viewmodel.VideoDetailsViewModel
import com.milimarty.vimeo.databinding.FragmentVideoDetailsBinding
import com.milimarty.vimeo.utils.Utils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VideoDetailsFragment : Fragment(R.layout.fragment_video_details) {

    private val viewModel by viewModels<VideoDetailsViewModel>()
    private var _mBinding: FragmentVideoDetailsBinding? = null
    private val mBinding
        get() = _mBinding!!

    /**
     * video details
     */
    private val args by navArgs<VideoDetailsFragmentArgs>()

    /**
     * initToolbar
     */
    private fun initToolbar() {
        mBinding.fragmentVideoDetailsToolbar.apply {
            setNavigationIcon(R.drawable.baseline_arrow_back_icon)
            setSubtitleTextColor(ContextCompat.getColor(context, R.color.black))
            setNavigationOnClickListener { findNavController().navigateUp() }
        }
    }

    /**
     * loadData
     */
    private fun loadData() {

        args.videoDetails.let {
            mBinding.apply {
                fragmentVideoDetailsDescriptionTextView.text = it.description
                fragmentVideoDetailsTitleTextView.text = it.title
                fragmentVideoDetailsLikesTextView.text =
                    it.metadata.connections.likes.total.toString()
                fragmentVideoDetailsCommentsTextView.text =
                    it.metadata.connections.comments.total.toString()
                fragmentVideoDetailsVideoDurationTextView.text = Utils.getVideoDuration(it.duration)
            }

        }

        Glide
            .with(mBinding.fragmentVideoDetailsVideoImageView)
            .load(args.videoDetails.imageWithPlayButtonUrl)
            .centerCrop()
            .transition(DrawableTransitionOptions.withCrossFade())
            .error(R.drawable.photo_place_holder_img)
            .into(mBinding.fragmentVideoDetailsVideoImageView)


        mBinding.fragmentVideoDetailsVideoImageView.setOnClickListener {
            mBinding.fragmentVideoDetailsLoading.isVisible = true

            viewModel.getVideoStreamConfig(args.videoDetails.videoId).observe(viewLifecycleOwner) {
                when (it) {
                    is NetworkResponse.ApiError -> {
                        mBinding.fragmentVideoDetailsLoading.isVisible = false
                        Toast.makeText(requireContext(),"Please check your internet connection ",Toast.LENGTH_SHORT).show()

                    }
                    is NetworkResponse.NetworkError -> {
                        mBinding.fragmentVideoDetailsLoading.isVisible = false
                        Toast.makeText(requireContext(),"Please check your internet connection ",Toast.LENGTH_SHORT).show()

                    }
                    is NetworkResponse.Success -> {
                        val action =
                            VideoDetailsFragmentDirections.actionNewsDetailsFragmentToVideoWatchingFragment(
                                it.body
                            )
                        findNavController().navigate(action)
                    }
                    is NetworkResponse.UnknownError -> {
                        mBinding.fragmentVideoDetailsLoading.isVisible = false
                        Toast.makeText(requireContext(),"Please check your internet connection ",Toast.LENGTH_SHORT).show()

                    }
                }
            }

        }


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _mBinding = FragmentVideoDetailsBinding.bind(view)
        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        initToolbar()
        loadData()

    }

    override fun onDestroy() {
        super.onDestroy()
        _mBinding = null
    }
}