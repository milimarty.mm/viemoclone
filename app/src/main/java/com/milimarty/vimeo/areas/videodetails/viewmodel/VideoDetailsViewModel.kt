package com.milimarty.vimeo.areas.videodetails.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.milimarty.vimeo.api.Response
import com.milimarty.vimeo.api.Status
import com.milimarty.vimeo.api.repo.VideosRepository
import com.milimarty.vimeo.areas.videodetails.NetworkResponse
import com.milimarty.vimeo.areas.videodetails.repo.VideoDetailsRepository
import com.milimarty.vimeo.areas.videoslist.model.VideoItemModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class VideoDetailsViewModel @Inject constructor(private val repository: VideoDetailsRepository) :
    ViewModel() {



    fun getVideoStreamConfig(videoId:Long): LiveData<NetworkResponse<String, Any>> {
        return repository.getVideoStreamConfig(videoId)
    }



}